package com.comtrade.repository;

import javax.xml.ws.RespectBinding;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.comtrade.entity.Artikal;

@Repository
public interface ArtikalRepository extends JpaRepository<Artikal, Integer>{

	
}
