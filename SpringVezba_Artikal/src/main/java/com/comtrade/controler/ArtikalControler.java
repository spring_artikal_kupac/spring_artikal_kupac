package com.comtrade.controler;

import javax.persistence.PostPersist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.comtrade.entity.Artikal;
import com.comtrade.service.ArtikalService;
	
@Controller
public class ArtikalControler {

	private ArtikalService artikalService;
	@Autowired
	public  ArtikalControler(ArtikalService artikalService) {
		super();
		this.artikalService=artikalService;
	}
	
	@GetMapping("/")
	public String index(Model model) {
		
		
		return "index";
	}
	
	@GetMapping("/artikal")
	public String artikalForma(Model model) {
		Artikal artikal = new Artikal();
		model.addAttribute("artikal", artikal);
		model.addAttribute("listArtikala",artikalService.listArtikala());
		
		return "artikal";
	}
	
	@PostMapping("/artikal/kreiraj")
	public String kreirajVlasnika(@ModelAttribute Artikal artikal) {
		artikalService.kreirajArtikal(artikal);
		
		return "redirect:/artikal";
		
	}
	
	
}
