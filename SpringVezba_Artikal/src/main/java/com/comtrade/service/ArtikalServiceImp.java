package com.comtrade.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.comtrade.entity.Artikal;
import com.comtrade.repository.ArtikalRepository;

@Service
public class ArtikalServiceImp implements ArtikalService {
	@Autowired
	private ArtikalRepository artikalRepository;
	@Override
	@Transactional
	public void kreirajArtikal(Artikal artikal) {
		artikalRepository.save(artikal);
		
	}
	@Override
	public List<Artikal> listArtikala() {
		
		return  artikalRepository.findAll();
	}

}
