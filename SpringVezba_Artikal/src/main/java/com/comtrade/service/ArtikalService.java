package com.comtrade.service;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.comtrade.entity.Artikal;


public interface ArtikalService {

	public void kreirajArtikal(Artikal artikal);

	public List<Artikal> listArtikala();
	
}
