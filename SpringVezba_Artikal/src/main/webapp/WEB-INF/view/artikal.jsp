<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<fieldset>
		<legend>Kreiraj Artikal</legend>
		<form:form action="/artikal/kreiraj" modelAttribute="artikal"
			method="post">


			<td>Naziv <form:input path="naziv" /></td>
			<br>
			<br>
			<td>Kolicina <form:input path="kolicina" /></td>
			<br>
			<br>
			<td>Cena <form:input path="cena" /></td>
			<br>
			<br>

			<td><input type="submit" value="Kreiraj artikal"></td>
			<br>

			</tr>



		</form:form>
	</fieldset>
	
	<fieldset>
		<legend>Lista Artikala</legend>
		
		
		<table>
				<tr>
					<th>Naziv</th>
					<th>Cena</th>
					<th>Kolicina</th>
				</tr>
				<c:forEach var="temp" items="${listArtikala}">
					<c:url var="updateLink" value="/artikal/${temp.id}">
						<c:param name="artikalId" value="${temp.id}" />
					</c:url>
				<tr>
						<td>${temp.naziv}</td>
						<td>${temp.cena}</td>
						<td>${temp.kolicina}</td>
						<td><a href="${updateLink}">Update</a></td>
						<!-- <td><a href=""></td> -->
				</tr>
				
				</c:forEach>
				
				
				
	</table>
		
		
		
		
		
		
		</fieldset>

</body>
</html>